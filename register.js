let registerForm = document.querySelector("#registerUser")

//user interaction such as loading the page, clicking on an HTML element, typing something, or submitting a form is called an event. 

//since we do not want the default behavior of our form submission event, we can do that

registerForm.addEventListener("submit", (e) => { //e in the parameters refers to the event itself (e is a placeholder and can be anything)
	e.preventDefault() //preventDefault prevents our form from reloaing the page when submitted

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	//validation to enable form submission only when all fields are populated, when passwords match, and when mobile number is exactly 11 numbers long
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
		
	// alert(`User ${firstName} ${lastName} with email ${email} and mobile number ${mobileNo} successfully registered`)

	//fetch by default sends GET requests and does not need to be configured for that. however, POST, PUT, and DELETE requests need the method option to be configured.
		
		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			}) 
		})
		.then(res => res.json())
 		.then(data => {
 			//console.log(`It is ${data} that there is a duplicate email`)

 			if(data){
 				alert("Duplicate email found. Please use a different email address.")
 			}else{
 				//ACTIVITY
 				//Create a fetch request in this else statement that allows our user to register 
 				//Properly handle the response so that a successful registration shows an alert that says registration is successful and NOT successful if not
 				
 			fetch('http://localhost:4000/users/register', {
				method: 'POST', 
				headers: {
					'Content-Type': 'application/json'
			},
				body: JSON.stringify({
				email: email,
				password: password
			}) 
		})
		.then(res => res.json())
 		.then(data => {
 			if(data){
 				alert("Successful Registration.")
 			}else{
 				alert("Registration NOT Successful")
 			}

 		})

// 			}else{
// 				alert("Please check your registration details and try again.")

// 	}
// })